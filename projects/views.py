from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.urls import reverse

from projects.models import Project


@login_required
def list_projects(request):
    projects = Project.objects.filter(members=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = "__all__"

    def get_success_url(self):
        return reverse("show_project", args=[self.object.id])


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"
