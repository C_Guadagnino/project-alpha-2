from django.urls import path

from projects.views import (
    list_projects,
    ProjectCreateView,
    ProjectDetailView,
)

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
    path("<int:pk>/", ProjectDetailView.as_view(), name="show_project"),
]
