from django.urls import path

from tasks.views import (
    CompleteTaskUpdateView,
    MyTasksListView,
    TaskCreateView,
)

urlpatterns = [
    path("mine/", MyTasksListView.as_view(), name="show_my_tasks"),
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path(
        "<int:pk>/complete/",
        CompleteTaskUpdateView.as_view(),
        name="complete_task",
    ),
]
